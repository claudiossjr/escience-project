import java.io.File
import java.util.Scanner
import java.util.concurrent.TimeUnit

import br.uff.spark.advancedpipe.FileGroupTemplate
import br.uff.spark.advancedpipe.{FileGroup, FileGroupTemplate}
import br.uff.spark.schema.SingleLineSchema
import org.apache.spark.{SparkConf, SparkContext}

import scala.collection.mutable.{ArrayBuffer, ListBuffer}
import scala.io.Source
import scala.util.control.Breaks._

object Main {

  val WORKSPACE = System.getenv("WORKSPACE")

  def parserInputFile(fileLocation: String): Seq[FileGroupTemplate] = {
    val scanner = new Scanner(new File(fileLocation))
    scanner.nextLine()
    val list = new ArrayBuffer[FileGroupTemplate]()
    while (scanner.hasNext) {
      val data = scanner.nextLine().split(";")
      val name = data(0)
      val year = data(1)
      val filePath = data(2)
      list += FileGroupTemplate.ofFile(new File(filePath), false, Map("NAME" -> name, "YEAR" -> year))
    }
    return list
  }

  def main(args: Array[String]) {

    val sc = new SparkContext(
      new SparkConf().setAppName("PlutoVis")
        .setScriptDir(WORKSPACE+"/scripts")
    )

    sc.fileGroup(parserInputFile(args(0)): _*)
      .runScientificApplication("RemoveSplitShape.cmd {{NAME}}_{{YEAR}}").setName("Remove Inner and Split Shapes")
      .runScientificApplication("SimplifyPoly.cmd {{NAME}}_{{YEAR}}").setName("Simplify Poly")
      .runScientificApplication("ConvertCoords.cmd {{NAME}}_{{YEAR}}").setName("Convert Coords")
      .runScientificApplication("MergeLots.cmd {{NAME}}_{{YEAR}}").setName("Merge Lots")
      .saveFilesAt(new File(WORKSPACE+"/output"))
      // .runScientificApplication("readseq.cmd {{NAME}} {{NAME}}.mafft").setName("Readseq").setSchema(new ReadSeqSchema)
      // .runScientificApplication("modelgenerator.cmd {{NAME}}").setName("Model Generator").setSchema(new ModelGeneratorSchema)
      // .runScientificApplication("raxml.cmd {{NAME}}.phylip {{NAME}}.mg.modelFromMG.txt").setName("Raxml").setSchema(new RaxmlSchema)
      

    sc.stop()
  }

}