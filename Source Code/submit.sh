echo "NAME;YEAR;FILE" > inputList.txt
echo "Manhattan;2002;$WORKSPACE/input/Manhattan_2002.extr" >> inputList.txt
echo "Manhattan;2003;$WORKSPACE/input/Manhattan_2003.extr" >> inputList.txt

$SPARK_HOME/bin/spark-submit \
  --class Main \
  --master local[4] \
  PlutoVisSpark.jar \
  inputList.txt
