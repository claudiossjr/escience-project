## TODO:
# - Read file .spl
# - Efetuar a conversão para lat/long
# - gerar arquigo .cvt
import sys, json
from pyproj import Proj


def __convertNYToGlobal(points):
    '''
    This function convert points coordinate from NY island to Global coordinate.
    '''
    globalProjection = Proj(init="esri:102718")
    new_points = [(globalProjection(point[0]*0.3048, point[1]*0.3048, inverse = True)) for point in points]
    return new_points

if __name__=='__main__':
  args = sys.argv[1:]

  if len(args) == 0:
    print("NO PARAMETER")
    exit(-1)

  _fileName = args[0]
  
  with  open(_fileName, "r") as fp_in:
    for line in fp_in:
      record = line.split(";")
      bblCode = record[0]
      _shapein = json.loads(record[1])
      _shape = __convertNYToGlobal(_shapein)
      print("{bbl};{shape}".format(bbl=bblCode, shape=json.dumps(_shape)))
  
