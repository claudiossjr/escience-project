# simplify each piece of shape geometry
# 1 - Read geometry shape
# 2 - simplpy it given a simplication factor
# 3 - Generate output file with file simplif

import sys, json
import shapely.geometry.polygon as shpoly
from shapely.geometry import Polygon

def __doPolySimplification(shape, simplification_threshold = 0.8):
  '''
  This function perform polygon simplification
  '''
  
  poly = Polygon(shape)
  poly = shpoly.orient(poly, -1)
  poly = Polygon(poly.simplify(simplification_threshold))
  poly = list(poly.exterior.coords)
  
  return poly

if __name__=='__main__':
  args = sys.argv[1:]

  if len(args) == 0:
    print("NO PARAMETER")
    exit(-1)

  _fileName = args[0]
  _simplifyFactor = 0.8
  
  if len(args) > 1:
    _simplifyFactor = float(args[1])

  if _simplifyFactor > 1 or _simplifyFactor < 0:
    print("ERROR: Simplification factor is wrong")
    exit(-1)

  with  open(_fileName, "r") as fp_in:
    for line in fp_in:
      record = line.split(";")
      bblCode = record[0]
      _shapein = json.loads(record[1])
      _shape = __doPolySimplification(_shapein, _simplifyFactor)
      print("{bbl};{shape}".format(bbl=bblCode, shape=json.dumps(_shape)))
  

