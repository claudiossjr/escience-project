import sys, json

if __name__=='__main__':
  args = sys.argv[1:]

  if len(args) == 0:
    print("NO PARAMETER")
    exit(-1)

  _fileName = args[0]

  bblShapeDict = {}

  with open(_fileName, "r") as fp_in:
    for line in fp_in:
      record = line.split(";")
      bblCode = record[0]
      _shape = json.loads(record[1])

      bblRaw = (bblCode.split("_"))[0]

      lotShape = bblShapeDict.get(bblRaw)
      if lotShape == None:
        lotShape = []
      lotShape.append(_shape)

      bblShapeDict[bblRaw] = lotShape

  fp_in.close()

  for _bbl in bblShapeDict:
    _shapes = bblShapeDict.get(_bbl)
    print("{bbl};{shape}".format(bbl=_bbl, shape=json.dumps(_shapes)))
  
