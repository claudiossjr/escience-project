# Read File for a given Name
# 1 - Read Lot Shape Structure from file
# 2 - Remove Inner Shapes.
# 3 - Generate output file with lot splitted.
  # If the lot has more than one polygon that describe its geometry. 

from shapely.geometry import Polygon
import sys, json


def __removeInnerShapes( shapes):
    '''
    This function get shape parts and shape points. 
    As results, it produces a list that contains each shape points.
    '''
    externalShapes = []
    removed = 0
    for shape in shapes:
      poly = Polygon(shape)
      # removing invalid shapes
      if not poly.is_valid:
        removed += 1
        continue
      external = True
      # Test if poly is inner poly
      for externalShape in externalShapes:
        externalPoly = Polygon(externalShape)
        if poly.within(externalPoly):
          external = False
          break
      # Test If poly is not inner from externals poly
      if external:
        externalShapes.append(shape)       
    
    return externalShapes

if __name__=='__main__':
  args = sys.argv[1:]
  if len(args) == 0:
    print("NO PARAMETERS")
    exit(-1)
  
  _fileName = args[0]
  
  with open(_fileName, "r") as fp_in:
    for line in fp_in:
      record = line.split(";")
      bblCode = record[0]
      shapes = json.loads(record[1])
      shapes = __removeInnerShapes(shapes)
      for index, _shape in enumerate(shapes):
        print("{bbl}_{inx};{shape}".format(bbl=bblCode, inx=index, shape=json.dumps(_shape)))
  fp_in.close()
